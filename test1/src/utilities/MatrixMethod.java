package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int [][] arr){
		
		//input arr {  { 1,2,3} , {4,5,6 } } 
		
		//output arrnew { { 1,1, 2, 2, 3, 3} , {4,4,5,5,6,6 } }
		int [][] newarr  = new int[arr.length][arr[0].length*2];
		
		for(int i=0; i<arr.length ;i++) {
			
			for(int j=0; j<arr[i].length;j++) {
				newarr [i][j*2] = arr[i][j];//[0][2] ---2
				newarr [i][j*2+1] = arr[i][j];//[0][3]----2
			}
			
		}
		
		return newarr;
		
	}
}

