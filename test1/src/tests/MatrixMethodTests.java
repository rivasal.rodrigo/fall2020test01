package tests;
import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void duplicateArray() {
		int [][] expect  = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
		int [][] input = {{1,2,3},{4,5,6}};//2x3
		
		assertArrayEquals(expect,MatrixMethod.duplicate(input));
	}

}
