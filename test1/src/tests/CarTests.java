package tests;

import automobiles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void negativeCarSpeed() {
		try {
		Car c = new Car(-10);
		}
		catch(IllegalArgumentException e){
			fail("Negative speed");
		}
	}
	
	@Test
	void carSpeed() {
		Car c = new Car(30);
		
		assertEquals(30,c.getSpeed());
	}
	
	@Test
	void carLocation() {
		Car c = new Car(30);
		assertEquals(50, c.getLocation());
	}
	
	@Test
	void carRight() {
		Car c = new Car(25);
		c.moveRight();
		assertEquals(75, c.getLocation());
	}
	
	@Test
	void carLeft() {
		Car c = new Car(60);
		c.moveLeft();
		assertEquals(0,c.getLocation());
		
	}
	
	@Test
	void carAccelerate() {
		Car c = new Car(30);
		c.accelerate();
		assertEquals(31,c.getSpeed());
	}
	
	@Test
	void carStop() {
		Car c = new Car(25);
		c.stop();
		assertEquals(0, c.getSpeed());
	}

}
